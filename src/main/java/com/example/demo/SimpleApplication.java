package com.example.demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;

@SpringBootApplication
public class SimpleApplication {

	static final Logger logger = LoggerFactory.getLogger(SimpleApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SimpleApplication.class, args);
		logger.info("App Started");
	}
}
