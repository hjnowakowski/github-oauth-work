package com.example.demo.controller;


import com.example.demo.exception.GithubContentNotResolved;
import com.example.demo.service.GitHubService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.info.ProjectInfoProperties;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.security.Principal;
import java.util.Collections;
import java.util.List;


@RestController
public class UserController {


    @Autowired
    GitHubService gitHubService;

    private static final Logger logger = LogManager.getLogger(UserController.class);


    @GetMapping("/user")
    public Principal user(Principal principal) {
        return principal;
    }

    @GetMapping("/email")
    public ResponseEntity<String> email() throws JSONException, GithubContentNotResolved{
        logger.info("Email Controller Method Called");
        if (gitHubService.githubEmail().equals("")){
            throw new GithubContentNotResolved("Error resolving Email from github");
        }
        return ResponseEntity.ok(gitHubService.githubEmail());
    }

    @GetMapping("/login")
    public ResponseEntity<String> login() throws JSONException, GithubContentNotResolved {
        logger.info("Login Controller Method Called");
        if (gitHubService.githubLogin().equals("")){
            throw new GithubContentNotResolved("Error resolving login from github.");
        }
        return ResponseEntity.ok(gitHubService.githubLogin());
    }

    @GetMapping("/repostatistics")
    public ResponseEntity<List<List<String>>> repoStatistics() throws JSONException, GithubContentNotResolved{
        logger.info("Repo Statistics Controller Method Called");
        if (gitHubService.repoStatistics().equals(Collections.emptyList())){
            throw new GithubContentNotResolved("Error resolving repository statistics from github.");
        }
        return ResponseEntity.ok(gitHubService.repoStatistics());
    }

    @GetMapping("/publicrepos")
    public ResponseEntity<List<String>> publicRepos(Principal principal) throws JSONException, GithubContentNotResolved{
        logger.info("Public Repos Controller Method Called");
        if(gitHubService.publicRepos(principal.getName()).equals(Collections.emptyList())){
            throw new GithubContentNotResolved("Error resolving public repositories from github.");
        }
        return ResponseEntity.ok(gitHubService.publicRepos(principal.getName()));
    }
}

