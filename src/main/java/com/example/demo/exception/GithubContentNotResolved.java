package com.example.demo.exception;

public class GithubContentNotResolved extends Exception {
    public GithubContentNotResolved(String message) {
        super(message);
    }
}
