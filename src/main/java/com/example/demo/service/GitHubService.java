package com.example.demo.service;

import org.springframework.boot.configurationprocessor.json.JSONException;

import java.util.List;


public interface GitHubService {
    String githubEmail() throws JSONException;

    String githubLogin() throws JSONException;

    List<List<String>> repoStatistics() throws JSONException;

    List<String> publicRepos(String username) throws JSONException;

}
