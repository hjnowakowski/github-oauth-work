package com.example.demo.service.impl;

import com.example.demo.service.GitHubService;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.configurationprocessor.json.JSONArray;
import org.springframework.boot.configurationprocessor.json.JSONException;
import org.springframework.boot.configurationprocessor.json.JSONObject;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


@Service
public class GitHubServiceImpl implements GitHubService {

    @Autowired
    OAuth2ClientContext oauth2ClientContext;

    private static final Logger logger = LogManager.getLogger(GitHubServiceImpl.class);

    private static final String GITHUB_API_USER_URL = "https://api.github.com/user?access_token=";

    private static final String GITHUB_API_REPOSITORY_URL = "https://api.github.com/user/repos?access_token=";

    private static final String GITHUB_API_PUBLIC_REPOSITORIES_BEGINNING = "https://api.github.com/users/";

    private static final String GITHUB_API_PUBLIC_REPOSITORIES_END = "/repos?access_token=";


    public String githubEmail() throws JSONException{

        try {

            OAuth2AccessToken accessToken = oauth2ClientContext.getAccessToken();

            RestTemplate restTemplate = new RestTemplate();

            String userResourceUrl
                    = GITHUB_API_USER_URL + accessToken;

            ResponseEntity<String> user
                    = restTemplate.getForEntity(userResourceUrl, String.class);

            final JSONObject userObject = new JSONObject(user.getBody());

            return userObject.getString("email");

        } catch (HttpClientErrorException e) {
            logger.error(e.getStatusCode());
            logger.error(e.getResponseBodyAsString());
            return "";
        }
    }

    @Override
    public String githubLogin() throws JSONException {
        try {

            OAuth2AccessToken accessToken = oauth2ClientContext.getAccessToken();

            RestTemplate restTemplate = new RestTemplate();

            String userResourceUrl
                    = GITHUB_API_USER_URL + accessToken;

            ResponseEntity<String> login
                    = restTemplate.getForEntity(userResourceUrl, String.class);

            final JSONObject userObject = new JSONObject(login.getBody());

            return userObject.getString("login");

        } catch (HttpClientErrorException e) {
            logger.error(e.getStatusCode());
            logger.error(e.getResponseBodyAsString());
            return "";
        }

    }

    @Override
    public List<String> publicRepos(String username) throws JSONException {

        List<String> publicReposList = new ArrayList<>();

        try {

            OAuth2AccessToken accessToken = oauth2ClientContext.getAccessToken();

            RestTemplate restTemplate = new RestTemplate();

            String userResourceUrl
                    = GITHUB_API_PUBLIC_REPOSITORIES_BEGINNING + username + GITHUB_API_PUBLIC_REPOSITORIES_END + accessToken;

            ResponseEntity<String> publicRepos
                    = restTemplate.getForEntity(userResourceUrl, String.class);

            final JSONArray publicReposArray = new JSONArray(publicRepos.getBody());

            for (int i = 0; i < publicReposArray.length(); i++) {

                publicReposList.add(publicReposArray
                        .getJSONObject(i)
                        .getString("name"));
            }

            return publicReposList;

        } catch (HttpClientErrorException e) {
            logger.error(e.getStatusCode());
            logger.error(e.getResponseBodyAsString());
            return Collections.emptyList();
        }
    }


    @Override
    public List<List<String>> repoStatistics() throws JSONException {

        List<List<String>> reposStats = new ArrayList<>();

        try {

            OAuth2AccessToken accessToken = oauth2ClientContext.getAccessToken();

            RestTemplate restTemplate = new RestTemplate();

            String userResourceUrl
                    = GITHUB_API_REPOSITORY_URL + accessToken;

            ResponseEntity<String> repos
                    = restTemplate.getForEntity(userResourceUrl, String.class);

            final JSONArray reposArray = new JSONArray(repos.getBody());

            for (int i = 0; i < reposArray.length(); i++) {

                reposStats.add(Arrays.asList(
                        reposArray.getJSONObject(i).getString("name"),
                        reposArray.getJSONObject(i).getString("size"),
                        reposArray.getJSONObject(i).getString("language")));

            }
            return reposStats;

        } catch (HttpClientErrorException e) {
            logger.error(e.getStatusCode());
            logger.error(e.getResponseBodyAsString());
            return Collections.emptyList();
        }

    }

}
